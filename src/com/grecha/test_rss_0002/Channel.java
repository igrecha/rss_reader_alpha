package com.grecha.test_rss_0002;

import java.util.Calendar;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Channel implements Parcelable {

	public static String CLASS_NAME = "com.grecha.test_rss.Channel";
	private String title;
	private String link;
	private String description;
	private ChannelImage image;
	private String pubDate;
	private String savedPage;
	private long listItemId;
	private byte[] logoImage;

	private class ChannelImage {
		public Bitmap pic;
		private String url;
		private String title;
		private String link;

		private ChannelImage(Bitmap pic, String url, String title, String link) {

			this.pic = pic;
			this.url = url;
			this.title = title;
			this.link = link;
		}

		private ChannelImage() {

			this.pic = null;
			this.url = "";
			this.title = "";
			this.link = "";
		}
	}

	public Channel(String title, String link, String description,
			ChannelImage image, String pubDate, String savedPage, long listItemId, byte[] logoImage) {
		this.title = title;
		this.link = link;
		this.description = description;
		if (image != null)
			this.image = image;
		else
			this.image = new ChannelImage();
		this.pubDate = pubDate;
		this.savedPage = savedPage;
		this.listItemId = listItemId;
		this.logoImage = logoImage;
	}

	public Channel() {
		this.title = "";
		this.link = "";
		this.description = "";
		this.image = new ChannelImage();
		Calendar c = Calendar.getInstance();
		this.pubDate = c.getTime().toString();
		this.savedPage = "";
		this.listItemId = -1;
		this.logoImage = new byte[1024];
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getImageUrl() {
		return image.url;
	}

	public void setImageUrl(String url) {
		this.image.url = url;
	}

	public String getImageTitle() {
		return image.title;
	}

	public void setImageTitle(String title) {
		this.image.title = title;
	}

	public String getImageLink() {
		return image.link;
	}

	public void setImageLink(String link) {
		this.image.link = link;
	}
	
	public String getSavedPage() {
		return savedPage;
	}
	
	public void setSavedPage(String savedPage) {
		this.savedPage = savedPage;
	}
	
	public long getListItemId() {
		return listItemId;
	}
	
	public void setListItemId(long listItemId) {
		this.listItemId = listItemId;
	}
	
	public byte[] getLogoImage() {
		return logoImage;
	}
	
	public void setLogoImage(byte[] logoImage) {
		this.logoImage = logoImage;
	}

	// Parcelable implementation
	private Channel(Parcel parcel) {
		this.title = parcel.readString();
		this.description = parcel.readString();
		this.link = parcel.readString();
		this.savedPage = parcel.readString();
		this.logoImage = new byte[parcel.readInt()];
		parcel.readByteArray(this.logoImage);
		this.listItemId = parcel.readLong();
		this.image = new ChannelImage();
		this.setImageUrl(parcel.readString());
		this.setPubDate(parcel.readString());
		
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int arg1) {

		parcel.writeString(this.getTitle());
		parcel.writeString(this.getDescription());
		parcel.writeString(this.getLink());
		parcel.writeString(this.getSavedPage());
		parcel.writeInt(this.logoImage.length);
		parcel.writeByteArray(this.getLogoImage());
		parcel.writeLong(this.getListItemId());
		parcel.writeString(this.getImageUrl());
		parcel.writeString(this.getPubDate());
	}

	public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
		public Channel createFromParcel(Parcel in) {
			return new Channel(in);
		}

		public Channel[] newArray(int size) {
			return new Channel[size];
		}
	};

	
}
