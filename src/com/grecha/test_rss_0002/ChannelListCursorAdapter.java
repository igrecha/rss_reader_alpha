package com.grecha.test_rss_0002;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.grecha.test_rss_0002.DBChannels.FeedEntry;

public class ChannelListCursorAdapter extends SimpleCursorAdapter {
	
	final String LOG_TAG = "myLogs";
	private static final int layout = R.layout.channel_message;
	private static final String[] from = { FeedEntry.COLUMN_NAME_TITLE,
			FeedEntry.COLUMN_NAME_DESCRIPTION };
	private static final int[] to = { R.id.tvChannelTitle,
			R.id.tvChannelDescription };

	public ChannelListCursorAdapter(Context context, Cursor cursor) {
		super(context, layout, cursor, from, to, FLAG_REGISTER_CONTENT_OBSERVER);
	}
	DBChannels dbChannels;

	@Override
	public void bindView(View view, Context context, Cursor c) {

		super.bindView(view, context, c);
		dbChannels = new DBChannels(context);
		dbChannels.open();
		int colIndex = c.getColumnIndex(FeedEntry.COLUMN_NAME_TITLE);
		String title = c.getString(colIndex);

		colIndex = c.getColumnIndex(FeedEntry.COLUMN_NAME_DESCRIPTION);
		String description = c.getString(colIndex);

		colIndex = c.getColumnIndex(FeedEntry.COLUMN_NAME_IMAGE_URL);
		String imageURL = c.getString(colIndex);
		
		colIndex = c.getColumnIndex(FeedEntry.COLUMN_NAME_IMAGE_BLOB);
		byte[] blobImage = c.getBlob(colIndex);
		Log.d(LOG_TAG, "blob = "+blobImage.toString());
		
		TextView tvChannelTitle = (TextView) view
				.findViewById(R.id.tvChannelTitle);
		TextView tvChannelDescription = (TextView) view
				.findViewById(R.id.tvChannelDescription);
		ImageView ivChannelImage = (ImageView) view
				.findViewById(R.id.imageChannel);

		if (tvChannelTitle != null) {
			tvChannelTitle.setText(title);
		}

		if (tvChannelDescription != null) {
			tvChannelDescription.setText(description);
		}
		
		//BLOB
		try {
		ByteArrayInputStream is = new ByteArrayInputStream(blobImage);
		Bitmap mBitmap = BitmapFactory.decodeStream(is);
		String path = Environment.getExternalStorageState()+ "/myAppDir/myImages/";
		File filename = new File(path, "myImg.png");
		filename.mkdirs();
		FileOutputStream out = new FileOutputStream(filename);
		mBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
		out.flush();
		out.close();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		Bitmap image = BitmapFactory.decodeByteArray(blobImage, 0, blobImage.length);

		ivChannelImage.setImageBitmap(image);
//		ByteArrayInputStream imageStream = new ByteArrayInputStream(blobImage);
//		Bitmap theImage = BitmapFactory.decodeStream(imageStream);
//		ivChannelImage.setImageBitmap(theImage);
		
//		if (ivChannelImage != null) {
//			// set default drawable
//			ivChannelImage.setImageDrawable(context.getResources().getDrawable(
//					R.drawable.rss_icon));
//			
//			// get image from source
////			ByteArrayInputStream imageStream = new ByteArrayInputStream(blobImage);
////			Bitmap theImage= BitmapFactory.decodeStream(imageStream);
////			ivChannelImage.setImageBitmap(theImage);
////			ImageDownloaderTask task = new ImageDownloaderTask(ivChannelImage);
////			task.execute(imageURL);
//
//		}
	}

	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
		return super.runQueryOnBackgroundThread(constraint);
	}

	private Bitmap loadChannelImage(String src) {

		InputStream input = null;
		Bitmap result = null;
		try {
			URL url = new URL(src);
			input = url.openStream();
			result = BitmapFactory.decodeStream(input);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (input != null)
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return result;
	}

	// from
	// http://android-developers.blogspot.com/2010/07/multithreading-for-performance.html
//	private class ImageDownloaderTask extends AsyncTask<String, Void, byte[]> {
//
//		private final WeakReference<ImageView> imageViewReference;
//		private String link;
//
//		public ImageDownloaderTask(ImageView imageView) {
//			imageViewReference = new WeakReference<ImageView>(imageView);
//		}
//
//		@Override
//		protected byte[] doInBackground(String... params) {
//
//			byte[] resultArray = null;
//			try {
//				DefaultHttpClient client = new DefaultHttpClient();
//				HttpGet request = new HttpGet(params[0]);
//				HttpResponse response = client.execute(request);
//				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//					HttpEntity entity = response.getEntity();
//					if (entity != null) {
//						resultArray = EntityUtils.toByteArray(entity);
//					}
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			byte[] result = resultArray;
//			return result;
//		}
//
//		@Override
//		protected void onPostExecute(byte[] result) {
//			ByteArrayInputStream imageStream = new ByteArrayInputStream(result);
//			Bitmap theImage = BitmapFactory.decodeStream(imageStream);
//			if (imageViewReference != null) {
//				ImageView imageView = imageViewReference.get();
//				if (imageView != null && theImage != null) {
//					imageView.setImageBitmap(theImage);
//					
//
//				}
//			}
//		}
//
//	}
}
