package com.grecha.test_rss_0002;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.util.Log;

public class DBChannels {
	final String LOG_TAG = "myLogs";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "FeedReader.db";

	private static final String TEXT_TYPE = " TEXT";
	private static final String COMMA_SEP = ",";
	private static final String BLOB_TYPE = " BLOB";

	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
			+ FeedEntry.TABLE_NAME + " (" + FeedEntry._ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP
			+ FeedEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP
			+ FeedEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP
			+ FeedEntry.COLUMN_NAME_LINK + TEXT_TYPE + COMMA_SEP
			+ FeedEntry.COLUMN_NAME_IMAGE_URL + TEXT_TYPE + COMMA_SEP
			+ FeedEntry.COLUMN_NAME_SAVED_PAGE + TEXT_TYPE + COMMA_SEP
			+ FeedEntry.COLUMN_NAME_IMAGE_BLOB + BLOB_TYPE + ");";

	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
			+ FeedEntry.TABLE_NAME;

	/* Inner class that defines the table Entry contents */
	public static abstract class FeedEntry implements BaseColumns {
		static final String TABLE_NAME = "entry";
		// private static final String COLUMN_NAME_ENTRY_ID = "entryid";
		static final String COLUMN_NAME_TITLE = "title";
		static final String COLUMN_NAME_DESCRIPTION = "description";
		static final String COLUMN_NAME_LINK = "ch_link";
		static final String COLUMN_NAME_IMAGE_URL = "ch_image_URL";
		static final String COLUMN_NAME_SAVED_PAGE = "saved_page";
		static final String COLUMN_NAME_IMAGE_BLOB = "image_blob";
	}

	// public static abstract class FeedItems implements BaseColumns {
	// static final String TABLE_NAME "table_tiems";
	//
	// }

	private DbHelper dbHelper;
	private SQLiteDatabase dbChannels;
	private Context context;

	public DBChannels(Context arg) {
		context = arg;
	}

	public void open() {
		dbHelper = new DbHelper(context);
		dbChannels = dbHelper.getReadableDatabase();
	}

	public void close() {
		if (dbHelper != null)
			dbHelper.close();
	}

	public Cursor getAllData() {

		return dbChannels.query(FeedEntry.TABLE_NAME, null, null, null, null,
				null, null);
	}

	public void removeAllDate() {
		dbChannels.delete(FeedEntry.TABLE_NAME, null, null);

	}

	public Cursor getRowWithId(int id) {

		return dbChannels
				.query(FeedEntry.TABLE_NAME, null, FeedEntry._ID + "=?",
						new String[] { Integer.toString(id) }, null, null, null);
	}

	// adding rec to db
	public void addRec(String title, String description, String link,
			String imageLink, String savedPage, byte[] logoImage) {
		ContentValues cv = new ContentValues();
		if (link != null) {
			cv.put(FeedEntry.COLUMN_NAME_TITLE, title);
			cv.put(FeedEntry.COLUMN_NAME_DESCRIPTION, description);
			cv.put(FeedEntry.COLUMN_NAME_LINK, link);
			cv.put(FeedEntry.COLUMN_NAME_IMAGE_URL, imageLink);
			cv.put(FeedEntry.COLUMN_NAME_SAVED_PAGE, savedPage);
			cv.put(FeedEntry.COLUMN_NAME_IMAGE_BLOB, logoImage);
			dbChannels.insert(FeedEntry.TABLE_NAME, null, cv);
		} else {
			Log.d(LOG_TAG, "here's the NPE!");
			throw new NullPointerException();
		}
	}
	
	public int updateRec(String savedPage, long id) {
		Log.d(LOG_TAG, "updateRec()");
		ContentValues cv = new ContentValues();
		cv.put(FeedEntry.COLUMN_NAME_SAVED_PAGE, savedPage);
		return dbChannels.update(FeedEntry.TABLE_NAME, cv,
					FeedEntry._ID + "=" +id, null);
	}


	public int removeRec(long id) {
		Log.d(LOG_TAG, "remove id = "+id);
		return dbChannels.delete(FeedEntry.TABLE_NAME,
				FeedEntry._ID + "=" + id, null);
	}

	//class for data transition in AsyncTask
	class Pair {
		String result;
		int positionId;
	}
	
	// Updater AsyncTask:
	class UpdateFeedTask extends AsyncTask<Object, Void, Pair> {

		protected Pair doInBackground(Object... params) {
			String response_str = "";
			String url_text = (String)params[0];
			int positionId = (Integer)params[1];
			Log.d(LOG_TAG, "updateRec():");
			Log.d(LOG_TAG, "input url: "+((String)params[0]));
			Log.d(LOG_TAG, "position id: "+((Integer)params[1]));

			try {
//				HttpClient httpClient = new DefaultHttpClient();
//				HttpContext localContext = new BasicHttpContext();
//				HttpGet httpGet = new HttpGet((String)params[0]);
//				
//				HttpResponse response = httpClient.execute(httpGet,
//						localContext);
//
//				BufferedReader reader = new BufferedReader(
//						new InputStreamReader(response.getEntity().getContent()));
//
//				String line = null;
//				while ((line = reader.readLine()) != null) {
//					result += line + "\n";
				
				HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(url_text);
                // Get the response
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                response_str = client.execute(request, responseHandler);
								

			} catch (IOException e) {
				e.printStackTrace();
			}
			Pair p = new Pair();
			p.result = response_str;
			p.positionId = positionId;
			return p;

		}

		protected void onPostExecute(Pair p) {
			Log.d(LOG_TAG, "text saved to result!");
			ContentValues cv = new ContentValues();
			cv.put(FeedEntry.COLUMN_NAME_SAVED_PAGE, p.result);
			int rowsCnt = dbChannels.update(FeedEntry.TABLE_NAME, cv,
					FeedEntry._ID + "=" +p.positionId, null);
			Log.d(LOG_TAG, "rows updated: "+rowsCnt);
			Log.d(LOG_TAG, "text saved to table");

		}

	}

	private class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context arg) {
			super(arg, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(SQL_CREATE_ENTRIES);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
			db.execSQL(SQL_DELETE_ENTRIES);
			onCreate(db);
		}
	}
}