package com.grecha.test_rss_0002;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

public class FeedPullParser {

	XmlPullParser xpp;
	ArrayList<Map<String, Object>> readedNews;
	Map<String, Object> mapedNews;
	Channel channel;
	InputStream inStr = null;
	InputStream secondInStr = null;
	final String LOG_TAG = "myLogs";

	// keys names
	static final String NEWS_TITLE = "news_title";
	static final String NEWS_DESCRIPTION = "news_desription";
	static final String NEWS_PUB_DATE = "pub_date";
	static final String NEWS_TAGS = "tags";
	static final String NEWS_LINK = "read_more_link";
	// static final String FULL_MESSAGE = "full_message";

	// names of the XML tags
	private static final String CHANNEL = "channel";
	private static final String IMAGE = "image";
	private static final String PUB_DATE = "pubDate";
	private static final String DESCRIPTION = "description";
	private static final String LINK = "link";
	private static final String TITLE = "title";
	private static final String ITEM = "item";
	private static final String URL = "url";
	private static final String TAG = "category";

	public FeedPullParser(String url) throws XmlPullParserException,
			MalformedURLException, IOException {
		
		if (url.startsWith("http://")) {
		
		URL feedURL = new URL(url);
		inStr = feedURL.openStream();
		secondInStr = feedURL.openStream();
		
		} else {
			inStr = new ByteArrayInputStream(url.getBytes("UTF-8"));
			secondInStr = new ByteArrayInputStream(url.getBytes("UTF-8")); // just added
			Log.d(LOG_TAG, "FeedPullParser: parse from String");
		}
		
		readedNews = new ArrayList<Map<String, Object>>();
		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		xpp = factory.newPullParser();
		xpp.setInput(inStr, null);
		
//		Log.d(LOG_TAG, allText.toString());
//		secondInStr.close();
	
	}

	public ArrayList<Map<String, Object>> parse()
			throws XmlPullParserException, IOException {
		Message msg = new Message();
		String tagName;
		String parentTag = "";
		String allText = convertStreamToString(secondInStr);
		
		Stack<String> rootTag = new Stack<String>();
		Log.d(LOG_TAG, "All text converted to String from stream");
		

		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {

			switch (eventType) {
			// start document
			case XmlPullParser.START_DOCUMENT:
//				String allText = convertStreamToString(inStr);
//				Log.d(LOG_TAG, allText.toString());
				break;
			// start tag
			case XmlPullParser.START_TAG:
				tagName = xpp.getName();

				rootTag.push(tagName);
				if (tagName.equalsIgnoreCase(ITEM)) {
					msg = new Message();
				} else if (tagName.equalsIgnoreCase(CHANNEL)) {
					channel = new Channel();
					channel.setSavedPage(allText);
				}
				break;
			// end tag
			case XmlPullParser.END_TAG:
				parentTag = "";
				tagName = xpp.getName();
//				Log.d(LOG_TAG, msg.getDescription().toString());
				if (tagName.equalsIgnoreCase(ITEM)) {
					mapedNews = new HashMap<String, Object>();
					mapedNews.put(NEWS_TITLE, msg.getTitle());
					mapedNews.put(NEWS_DESCRIPTION, msg.getDescription().replaceAll("<br/>", "").replaceAll("<img.+?>", ""));
					mapedNews.put(NEWS_PUB_DATE, msg.getPubDate());
					mapedNews.put(NEWS_TAGS, msg.getTags());
					mapedNews.put(NEWS_LINK, msg.getLink());
					// mapedNews.put(FULL_MESSAGE, msg);
					readedNews.add(mapedNews);
				}
				if (tagName.equalsIgnoreCase(rootTag.readFirst()))
					rootTag.pull();
				break;
			// print text
			case XmlPullParser.TEXT:				
				for (String str : rootTag) {
					parentTag = str;
					if (str.equalsIgnoreCase(CHANNEL)
							|| str.equalsIgnoreCase(ITEM)
							|| str.equalsIgnoreCase(IMAGE)) {
						break;
					}
				}
				// processing last opened tag
				String rootTagName = rootTag.readFirst();
				if (rootTagName.equalsIgnoreCase(TITLE)) {
					// processing root element for tag "TITLE"
					if (parentTag.equalsIgnoreCase(ITEM)) {
						msg.setTitle(xpp.getText());
					} else if (parentTag.equalsIgnoreCase(CHANNEL)) {
						channel.setTitle(xpp.getText());
					} else if (parentTag.equalsIgnoreCase(IMAGE)) {
						channel.setImageTitle(xpp.getText());
					}
				} else if (rootTagName.equalsIgnoreCase(DESCRIPTION)) {
					// processing root element for tag "DESCRIPTION"
					if (parentTag.equalsIgnoreCase(ITEM)) {
						msg.setDescription(xpp.getText());
					} else if (parentTag.equalsIgnoreCase(CHANNEL)) {
						channel.setDescription(xpp.getText());
					}
				} else if (rootTagName.equalsIgnoreCase(LINK)) {
					// processing root element for tag "LINK"
					if (parentTag.equalsIgnoreCase(ITEM)) {
						msg.setLink(xpp.getText());
					} else if (parentTag.equalsIgnoreCase(CHANNEL)) {
						channel.setLink(xpp.getText());
					} else if (parentTag.equalsIgnoreCase(IMAGE)) {
						channel.setImageLink(xpp.getText());
					}
				} else if (rootTagName.equalsIgnoreCase(URL)) {
					// // processing root element for tag "URL"
					if (parentTag.equalsIgnoreCase(IMAGE)) {

						channel.setImageUrl(xpp.getText());
					}
				} else if (rootTagName.equalsIgnoreCase(PUB_DATE)) {
					// processing root element for tag "PUB_DATE"
					if (parentTag.equalsIgnoreCase(ITEM)) {
						msg.setPubDate(xpp.getText());
					} else if (parentTag.equalsIgnoreCase(CHANNEL)) {
						channel.setPubDate(xpp.getText());
					}
				} else if (rootTagName.equalsIgnoreCase(TAG)) {
					// processing root element for tag "TAG"
					if (parentTag.equalsIgnoreCase(ITEM)) {
						msg.addTag(xpp.getText());
					}
				}
				break;

			default:
				break;
			}
			eventType = xpp.next();
		}

		return readedNews;
	}

	public Channel getChannel() {
		return channel;
	}

	public void close() {
		try {
			inStr.close();
			secondInStr.close();
			Log.d(LOG_TAG, "Parser clodes");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		xpp = null;
		mapedNews = null;
		readedNews = null;
		channel = null;
	}
	
	//Read/convert an InputStream to a String
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
}
