package com.grecha.test_rss_0002;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;

import com.grecha.test_rss_0002.DBChannels.FeedEntry;

public class FeedReader extends ListActivity {

	final static String LOG_TAG = "myLogs";

	static final String IN_DATE_FORMAT = "EE, dd MMM yyyy HH:mm:ss zzz";
	static final String OUT_DATE_FORMAT = "EE, dd MMMM HH:mm";

	// context menu items ID
	static final int MENU_ITEM_REMOVE = 1;
	static final int MENU_ITEM_UPDATE = 2;

	// request codes
	static final int REQUEST_CODE_LIST_ITEM_CLICK = 1;
	static final int REQUEST_CODE_DIALOD_TRY_AGAIN = 2;
	static final int REQUEST_CODE_DIALOG_ADD_CHANNEL = 3;

	DBChannels dbChannels;
	ChannelListCursorAdapter scAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed_reader);

		dbChannels = new DBChannels(this);
		dbChannels.open();
		Cursor cursor = dbChannels.getAllData();
		scAdapter = new ChannelListCursorAdapter(this, cursor);
		setListAdapter(scAdapter);
		registerForContextMenu(getListView());
	}

	protected void onRestart() {
		Cursor cursor = dbChannels.getAllData();
		scAdapter.changeCursor(cursor);
		super.onRestart();
	}

	protected void onStop() {
		scAdapter.getCursor().close();
		super.onStop();
	}

	protected void onDestroy() {
		dbChannels.close();
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.feed_reader, menu);
		return true;
	}

	// work with ActionBar items
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_add_channel:
			// open add channel dialog
			getChannelLink();
			return true;

		case R.id.menu_remove_all:
			// remove all entries from database
			dbChannels.removeAllDate();
			Cursor cursor = dbChannels.getAllData();
			scAdapter.changeCursor(cursor);

		case R.id.menu_update:
			// update all channels

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Context menu
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(Menu.NONE, MENU_ITEM_REMOVE, Menu.NONE, "Remove");
		menu.add(Menu.NONE, MENU_ITEM_UPDATE, Menu.NONE, "Update");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		int id = item.getItemId();
		boolean result = true;
		switch (id) {

		case MENU_ITEM_REMOVE:
			AdapterContextMenuInfo adapter = (AdapterContextMenuInfo) item
					.getMenuInfo();
			int rowsCnt = dbChannels.removeRec(adapter.id);

			Cursor cursor = dbChannels.getAllData();
			scAdapter.changeCursor(cursor);
			result = true;
			break;
		case MENU_ITEM_UPDATE:
			adapter = (AdapterContextMenuInfo) item.getMenuInfo();
			cursor = dbChannels.getAllData();
			int cursorPosition = (int) adapter.id;
			Log.d(LOG_TAG, "position id = " + cursorPosition);

			cursor = dbChannels.getRowWithId(cursorPosition);
			cursor.moveToFirst();
			// cursor.moveToPosition(cursorPosition);
			Log.d(LOG_TAG,
					"after getRowWithId position id = " + cursor.getPosition());

			int cloumnLinkId = cursor
					.getColumnIndex(FeedEntry.COLUMN_NAME_LINK);
			int cloumnSavedPageId = cursor
					.getColumnIndex(FeedEntry.COLUMN_NAME_SAVED_PAGE);
			String channelLink = cursor.getString(cloumnLinkId);
			// dbChannels.updateRec(adapter.id);
			dbChannels.new UpdateFeedTask()
					.execute(channelLink, cursorPosition);
			cursor = dbChannels.getAllData();
			scAdapter.changeCursor(cursor);
			result = true;
			break;

		default:
			result = super.onContextItemSelected(item);
		}
		return result;
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		Channel ch = new Channel();
		Cursor cursor = dbChannels.getRowWithId((int) id);
		cursor.moveToFirst();
		int colIndex = 0;

		colIndex = cursor.getColumnIndex(FeedEntry.COLUMN_NAME_TITLE);
		ch.setTitle(cursor.getString(colIndex));

		colIndex = cursor.getColumnIndex(FeedEntry.COLUMN_NAME_DESCRIPTION);
		ch.setDescription(cursor.getString(colIndex));

		colIndex = cursor.getColumnIndex(FeedEntry.COLUMN_NAME_LINK);
		ch.setLink(cursor.getString(colIndex));

		colIndex = cursor.getColumnIndex(FeedEntry.COLUMN_NAME_SAVED_PAGE);
		ch.setSavedPage(cursor.getString(colIndex));

		ch.setListItemId(id);
		// ��� �� ��
		// Log.d(LOG_TAG, "from FeedReader "+ ch.getSavedPage());

		showChannelEntrys(ch, REQUEST_CODE_LIST_ITEM_CLICK, true);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			Log.d(LOG_TAG, "onActivityResult: RESULT_OK");
			if (data != null) {
				// add new channel to database if click menu "Add" button
				if (requestCode == REQUEST_CODE_DIALOG_ADD_CHANNEL) {
					Channel ch = data.getParcelableExtra(Channel.CLASS_NAME);

//					byte[] logoImage = getLogoImage(ch.getImageUrl());
					dbChannels.addRec(ch.getTitle(), ch.getDescription(),
							ch.getLink(), ch.getImageUrl(), ch.getSavedPage(), ch.getLogoImage());
					Log.d(LOG_TAG, "addRec to database");
					

				} // add if statment for REQUEST_CODE_LIST_ITEM_CLICK
				if (requestCode == REQUEST_CODE_LIST_ITEM_CLICK) {
					Channel ch = data.getParcelableExtra(Channel.CLASS_NAME);
					Log.d(LOG_TAG, "listItemId: " + ch.getListItemId());
					int rowcCnt = dbChannels.updateRec(ch.getSavedPage(),
							ch.getListItemId());

				}
			}
		} else {

			Log.d(LOG_TAG, "else....");
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			// Exception e =
			// data.getParcelableExtra(ShowChannelActivity.RETURN_EXC);
			builder.setTitle(R.string.connection_failed);
			String link = "this channel ";
			if (data != null) {
				final Channel ch = data.getParcelableExtra(Channel.CLASS_NAME);
				link = ch.getLink();
				// set positive button only if intent has data
				builder.setPositiveButton(R.string.yes,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								showChannelEntrys(ch,
										REQUEST_CODE_DIALOD_TRY_AGAIN, true);
							}
						});
			}
			builder.setMessage(getResources().getString(R.string.try_again)
					+ " " + link + "?");

			builder.setNegativeButton(R.string.no,
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();

		}
	}

	// Open dialog and get link to channel
	private void getChannelLink() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		LayoutInflater inflater = getLayoutInflater();

		builder.setMessage(R.string.add_channel_text);
		View addChannelView = inflater.inflate(R.layout.add_channel, null);
		final EditText etLink = (EditText) addChannelView
				.findViewById(R.id.et_channel_link);
		builder.setView(addChannelView);
		builder.setPositiveButton(R.string.add_channel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Channel newChannel = new Channel();
						newChannel.setLink(etLink.getText().toString());
						showChannelEntrys(newChannel,
								REQUEST_CODE_DIALOG_ADD_CHANNEL, true);
					}
				});
		builder.setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void showChannelEntrys(Channel ch, int requestCode,
			boolean getResult) {
		Intent intent = new Intent(this, ShowChannelActivity.class);
		intent.putExtra(Channel.CLASS_NAME, ch);
		if (getResult)
			startActivityForResult(intent, requestCode);
		else
			startActivity(intent);
	}
	
	//This code will take a image from url and convert is to a byte array
	
	//byte[] logoImage = getLogoImage(IMAGEURL);
//	private byte[] getLogoImage(String url){
//	     try {
//	             URL imageUrl = new URL(url);
//	             URLConnection ucon = imageUrl.openConnection();
//
//	             InputStream is = ucon.getInputStream();
//	             BufferedInputStream bis = new BufferedInputStream(is);
//
//	             ByteArrayBuffer baf = new ByteArrayBuffer(500);
//	             int current = 0;
//	             while ((current = bis.read()) != -1) {
//	                     baf.append((byte) current);
//	             }
//
//	             return baf.toByteArray();
//	     } catch (Exception e) {
//	             Log.d("ImageManager", "Error: " + e.toString());
//	     }
//	     return null;
//	}

}