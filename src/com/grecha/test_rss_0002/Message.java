package com.grecha.test_rss_0002;

import java.util.Calendar;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Comparable<Message>, Parcelable {
	
	public static String CLASS_NAME = "com.grecha.test_rss.Channel";
	private String title;
	private String description;
	private String link;
	private String pubDate;
	private String tags;
	
	public Message(String title, String description, String link, String pubDate, String tags) {
		this.title = title;
		this.description = description;
		this.link = link;
		this.pubDate = pubDate;
		this.tags = tags;
	}

	public Message() {
		this.title = "";
		this.description = "";
		this.link = "";
		Calendar c = Calendar.getInstance();
		this.pubDate = c.getTime().toString();
		this.tags = "";
	}
	
	public String getTitle() {
		if (title != null)
			return title;
		else
			return "";
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		if (description != null)
			return description;
		else
			return "";
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		if (link != null)
			return link;
		else
			return "";
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	public void addTag(String tag) {
		if (this.tags != null){
			if (this.tags.length() == 0){
				this.tags += tag;
			} else {
				this.tags += ", " + tag;
			}
		}
	}
	
	public String toString() {
		/*
		 * return "\n*********************\n" + "Message: \n title= " + title +
		 * "\n description= " + description + "\n link= " + link +
		 * "\n pubDate= " + pubDate + "\n";
		 */
		return getTitle();
	}
	
	// Parcelable implementation
		private Message(Parcel parcel) {
			
			this.title = parcel.readString();
			this.description = parcel.readString();
			this.link = parcel.readString();
			this.pubDate = parcel.readString();
			this.tags = parcel.readString();
		}
	
	public String getTags(){
		return this.tags;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel parcel, int arg1) {
		parcel.writeString(this.getTitle());
		parcel.writeString(this.getDescription());
		parcel.writeString(this.getLink());
		parcel.writeString(this.getPubDate());
		parcel.writeString(this.getTags());
	}
	
	@Override
	public int compareTo(Message another) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
		public Message createFromParcel(Parcel in) {
			return new Message(in);
		}

		public Message[] newArray(int size) {
			return new Message[size];
		}
	};
}
