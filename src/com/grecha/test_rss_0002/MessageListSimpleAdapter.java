package com.grecha.test_rss_0002;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.text.Html;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class MessageListSimpleAdapter extends SimpleAdapter {

	private Locale locale_in_parser;
	private Locale locale_out_parser;

	private SimpleDateFormat sdf_in;
	private SimpleDateFormat sdf_out;

	public MessageListSimpleAdapter(Context context,
			List<Map<String, Object>> data, int resource, String[] from,
			int[] to) {
		super(context, data, resource, from, to);
		locale_in_parser = Locale.ENGLISH;
		locale_out_parser = context.getResources().getConfiguration().locale;
		sdf_in = new SimpleDateFormat(FeedReader.IN_DATE_FORMAT,
				locale_in_parser);
		sdf_out = new SimpleDateFormat(FeedReader.OUT_DATE_FORMAT,
				locale_out_parser);
	}

	public void setViewText(TextView v, String text) {
		int id = v.getId();
		switch (id) {
		case R.id.tvChannelDescription:
			text = text.replaceAll("<br>", "");
			text = text.replaceAll("<(.*?)\\>", " ");
			text = text.replaceAll("<br/>", "");
			text = text.replaceAll("<img.+?>", "");
			text = text.replaceAll("<(.*?)\\\n", " ");
			text = text.replaceFirst("(.*?)\\>", " ");
			text = text.replaceAll("&amp;", " ");
			text = text.replaceAll("&nbsp;", " ");

			v.setText(Html.fromHtml(text));
			break;
		case R.id.tvPubDate:
			String str = "";
			try {
				Date date = sdf_in.parse(text);
				str = sdf_out.format(date);

			} catch (java.text.ParseException e) {

				e.printStackTrace();
				str = text;
			}
			v.setText(str);
			break;
		default:
			v.setText(text);
		}
	}
}
