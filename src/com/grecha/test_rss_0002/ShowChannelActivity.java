package com.grecha.test_rss_0002;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

public class ShowChannelActivity extends ListActivity {
	final String LOG_TAG = "myLogs";
	ArrayList<Map<String, Object>> news;
	private Channel returnChannel;
	private Channel activeChannel;
	SimpleAdapter adapter;
	ProgressBar progressBar;
	public static String RETURN_EXC = "show_ch_activity_exception";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_channel);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);

		Bundle b = getIntent().getExtras();
		activeChannel = b.getParcelable(Channel.CLASS_NAME);

		// ���� �� ��������
		// Log.d(LOG_TAG,
		// "from ShowChannelActivity() "+activeChannel.getSavedPage());
		String savedPage = activeChannel.getSavedPage();
		// Log.d(LOG_TAG, "ShowChannelActivity() : "+savedPage);

		if (isNetworkAvailable()) {
			NewsDownloader newsDownloader = new NewsDownloader(this);
			newsDownloader.execute(activeChannel.getLink());
		} else {
			NewsDownloader newsDownloader = new NewsDownloader(this);
			newsDownloader.execute(savedPage);
		}
	}

	// Checks, if internet is avilable
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		Log.d(LOG_TAG, "onListItemClick");
		Map<String, Object> m = news.get(position);
		String str = (String) m.get(FeedPullParser.NEWS_DESCRIPTION);
		String link = (String) m.get(FeedPullParser.NEWS_LINK);
		Intent intent = new Intent(this, ShowMessageActivity.class);
		intent.putExtra(FeedPullParser.NEWS_DESCRIPTION, str);
		intent.putExtra(FeedPullParser.NEWS_LINK, link);
		startActivity(intent);

	}

	private class NewsDownloader extends
			AsyncTask<String, Void, ArrayList<Map<String, Object>>> {

		private FeedPullParser parser = null;
		private ArrayList<Map<String, Object>> result = null;
		private Context context;
		private Exception e = null;

		public NewsDownloader(Context c) {
			context = c;
			Log.d(LOG_TAG, "NewsDownloader executed");
		}

		@Override
		protected ArrayList<Map<String, Object>> doInBackground(
				String... params) {
			byte[] resultArray = null;
			try {

//				DefaultHttpClient client = new DefaultHttpClient();
//				HttpGet request = new HttpGet(activeChannel.getImageLink());
//				HttpResponse response = client.execute(request);
//				HttpEntity entity = response.getEntity();
//				int imageLength = (int) (entity.getContentLength());
//				InputStream is = entity.getContent();
//				byte[] imageBlob = new byte[imageLength];
//				int bytesRead = 0;
//				while (bytesRead < imageLength) {
//					int n = is.read(imageBlob, bytesRead, imageLength
//							- bytesRead);
//					if (n <= 0)
//						; // do some error handling
//					bytesRead += n;
//				}
				
				
				//works
				DefaultHttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet(params[0]);
				HttpResponse response = client.execute(request);
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					HttpEntity entity = response.getEntity();
					if (entity != null) {
						resultArray = EntityUtils.toByteArray(entity);
					}
				}
				
//				URL imageUrl = new URL(params[0]);
//	             URLConnection ucon = imageUrl.openConnection();
//
//	             InputStream is = ucon.getInputStream();
//	             BufferedInputStream bis = new BufferedInputStream(is);
//
//	             ByteArrayBuffer baf = new ByteArrayBuffer(500);
//	             int current = 0;
//	             while ((current = bis.read()) != -1) {
//	                     baf.append((byte) current);
//	             }
//
//	             resultArray =  baf.toByteArray();
			 
				parser = new FeedPullParser(params[0]);
				result = parser.parse();
				returnChannel = parser.getChannel();
				returnChannel.setLink(activeChannel.getLink());
				returnChannel.setListItemId(activeChannel.getListItemId());
				byte[] result = resultArray;
				returnChannel.setLogoImage(result);
				Log.d(LOG_TAG, "byte[] logoImage: "+result.toString());
			
			} catch (XmlPullParserException e) {
				e.printStackTrace();
				this.e = e;
			} catch (MalformedURLException e) {
				e.printStackTrace();
				this.e = e;
			} catch (IOException e) {
				e.printStackTrace();
				this.e = e;
			} finally {
				if (parser != null)
					parser.close();

			}
			return result;
		}

		protected void onPostExecute(ArrayList<Map<String, Object>> result) {
			// if result isn't null try to show result
			if (result != null) {
				Intent intent = new Intent();
				intent.putExtra(Channel.CLASS_NAME, returnChannel);
				setResult(RESULT_OK, intent);

				news = result;
				MessageListSimpleAdapter adapter = new MessageListSimpleAdapter(
						context, news, R.layout.news_message, new String[] {
								FeedPullParser.NEWS_TITLE,
								FeedPullParser.NEWS_DESCRIPTION.replaceAll(
										"<br>", ""),
								FeedPullParser.NEWS_PUB_DATE,
								FeedPullParser.NEWS_TAGS }, new int[] {
								R.id.tvTitle, R.id.tvDescription,
								R.id.tvPubDate, R.id.tvTags });
				progressBar.setVisibility(View.GONE);
				getListView().setAdapter(adapter);
				// if result is null read exception type and send to the "top"
				// activity
			} else {
				Log.d(LOG_TAG,
						"else in onPostExecute() ShowChannelActivity.class");
				Intent intent = new Intent();
				intent.putExtra(Channel.CLASS_NAME, activeChannel);
				intent.putExtra(RETURN_EXC, this.e);
				setResult(RESULT_CANCELED, intent);
				finish();
			}
		}

	}
}