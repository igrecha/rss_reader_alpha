package com.grecha.test_rss_0002;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ShowMessageActivity extends Activity {
	private static int MENU_READ_MORE = 1;

	private String link;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_news);
		
		TextView tv = (TextView)findViewById(R.id.fullMessage);
		tv.setMovementMethod(ScrollingMovementMethod.getInstance());
		tv.setMovementMethod(LinkMovementMethod.getInstance());
		
		Bundle b = getIntent().getExtras();
		String str = b.getString(FeedPullParser.NEWS_DESCRIPTION);
		link = b.getString(FeedPullParser.NEWS_LINK);
		
		tv.setText(Html.fromHtml(str, new ImageGetter() {
			public Drawable getDrawable(String source) {
				// cut all images and show only 1px clear image
				return getResources().getDrawable(R.drawable.px_clear);
			}
		}, null));
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, MENU_READ_MORE, Menu.NONE, "Read more");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result;
		if (item.getItemId() == MENU_READ_MORE) {
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(link));
			startActivity(i);
			result = true;
		} else {
			result = super.onOptionsItemSelected(item);
		}
		return result;
	}
}
