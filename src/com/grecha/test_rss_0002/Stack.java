package com.grecha.test_rss_0002;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Stack<Item> implements Iterable<Item> {

	private Node head;
	private int depth;

	private class Node {
		private Item item;
		private Node next;
	}

	public Stack() {
		head = null;
		depth = 0;
	}

	public void push(Item item) {
		if (item == null)
			throw new NullPointerException("Incorrect input");
		if (isEmpty()) {
			head = new Node();
			head.item = item;
			head.next = null;
		} else {
			Node newHead = new Node();
			newHead.item = item;
			newHead.next = head;
			head = newHead;
		}
		depth += 1;
	}

	public Item pull() {
		if (isEmpty())
			throw new NoSuchElementException("Stack is empty");
		Item result = head.item;
		head = head.next;
		depth -= 1;
		return result;
	}

	public Item readFirst() {
		if (isEmpty())
			throw new NoSuchElementException("Stack is empty");
		return head.item;
	}

	public boolean isEmpty() {
		return head == null;
	}

	public Iterator<Item> iterator() {
		return new ListIterator();
	}

	// an iterator, doesn't implement remove() since it's optional
	private class ListIterator implements Iterator<Item> {
		private Node current = head;

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext())
				throw new NoSuchElementException();
			Item item = current.item;
			current = current.next;
			return item;
		}
	}
}
